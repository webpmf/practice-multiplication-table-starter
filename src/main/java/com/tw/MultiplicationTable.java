package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if(isValid(start, end)) {

        }
        String table = generateTable(start, end);
        return table;
    }

    public Boolean isValid(int start, int end) {
        boolean result = isStartNotBiggerThanEnd(start, end) && isInRange(start) && isInRange(end) ;
        return result;
    }

    public Boolean isInRange(int number) {
        return number < 1000 && number > 0;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        return start <= end;
    }

    public String generateTable(int start, int end) {
        String table = "";
        for (int i = start;i <= end;i++){
            table = table + generateLine(start,i) + "\n";
        }
        return table.trim();
    }

    public String generateLine(int start, int row) {
        String line = "";
        for (int i = start;i <= row;i++){
            line = line + generateSingleExpression(i,row) + "  ";
        }
        return line.trim();
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        String expression = multiplicand + "*" + multiplier + "=" + multiplicand*multiplier;
        return expression;
    }
}
